// 	Aggregate to count the total number of items supplied by Yellow Farms and has a price lower than 50. ($count)
db.fruits.aggregate([

	{$match: {supplier: "Yellow Farms",price:{$lt:50}}},
	{$count: "totalItemsOfYellowFarmsPriceLess50"}

])

// 	Aggregate to count the total number of items with price lesser than 30. ($count)
db.fruits.aggregate([

	{$match: {price:{$lt:30}}},
	{$count: "totalItemsPriceLess30"}

])

// 	Aggregate to get the average price of fruits supplied by Yellow Farm. ($group)
db.fruits.aggregate([

	{$match: {supplier: "Yellow Farms"}},
	{$group: {_id:"$supplier",avgPrice:{$avg: "$price"}}}

])

// Aggregate to get the highest price of fruits supplied by Red Farm Inc. ($group)
db.fruits.aggregate([

	{$match: {supplier: "Red Farms Inc."}},
	{$group: {_id:"$supplier",maxPrice:{$max: "$price"}}}

])

// Aggregate to get the lowest price of fruits supplied by Red Farm Inc. ($group)
db.fruits.aggregate([

	{$match: {supplier: "Red Farms Inc."}},
	{$group: {_id:"$supplier",minPrice:{$min: "$price"}}}

])



