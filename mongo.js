db.fruits.insertMany([
    {
	name: "Apple",
	supplier: "Red Farms Inc.",
	stocks: 20,
	price: 40,
	onSale: true
    },
    {
	name: "Banana",
	supplier: "Yellow Farms",
	stocks: 15,
	price: 20,
	onSale: true
    },
    {
	name: "Kiwi",
	supplier: "Green Farming and Canning",
	stocks: 25,
	price: 50,
	onSale: true
    },
    {
	name: "Mango",
	supplier: "Yellow Farms",
	stocks: 10,
	price: 60,
	onSale: true
    },
    {
	name: "Dragon Fruit",
	supplier: "Red Farms Inc.",
	stocks: 10,
	price: 60,
	onSale: true
    }
])

/*
	Aggregation Pipeline Stages

	Aggregation is typically done in 2-3 steps. Each process in aggreagation is called a stage.

	$match and $group is one of the common stages in aggregate but there are more.

	$match - is used to match or get documents which satisfies the condition
	Syntax: {$match: {field:<value>}}

	$group - allows us to group together documents and create an analysis out of the grouped documents.

	_id: in the group stage, essentially associates an id to our results.
	_id: also determines the number of groups

	_id: "$supplier" - essentially group together documents with the same values in the supplier field.
	_id: $<field> groups documents based on the value of the indicated field.
*/

db.fruits.aggregate([

	// looked for and got all fruits that are onSale.
    {$match: {onSale:true}},

    /*
    	Match:
		apple
		supplier: Red Farms Inc

		banana
		supplier: Yellow Farms

		mango
		supplier: Yellow Farms

		kiwi:
		supplier: Green Farming

		dragon fruit
		supplier: Red Farms Inc

		group1:
		-id: Red Farms Inc
		apple
		dragon fruit

		group2:
		_id: Yellow Farms
		banana
		mango

		group3:
		_id: Green Farming
		kiwi

		$sum is used to add or total the values of the given field

		group1
		apple
		stocks:20
		dragonfruit
		stocks: 10
		$sum: 30

		group2
		banana
		stocks: 15
		mango
		stocks: 10
		$sum: 25

		group3
		kiwi
		stocks: 25
		$sum: 25


    */
    {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

	// If the _id's value is definite or given, $group will only create one group
	{$match: {onSale:true}},
	{$group: {_id:null,totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"AllOnSaleFruits",totalStocks:{$sum:"$stocks"}}}

])

// Mini-Activity
//Get the total stocks of all products onSale supplied by Yellow Farms.
// Solution:
db.fruits.aggregate([

	{$match: {supplier:"Yellow Farms",onSale:true}},
	{$group: {_id:"$name",totalStocks:{$sum:"$stocks"}}}

])

// $avg - is an operator used in $group stage.
// $avg gets the avg of the numerical values of the indicated field in grouped documents

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"$supplier",avgStock:{$avg:"$stocks"}}}

])

// result: _id = null - 1 group and avgPrice of all fruits on sale.
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:null,avgPrice:{$avg:"$price"}}}

])

// $max - allows us to get the highest value out of all the values in a given field per group.

// highest number of stock for all items on sale.
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"highestStockOnSale", maxStock:{$max: "$stocks"}}}

])

// highest price for all items on sale
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null, maxPrice: {$max: "$price"}}}	

])

// $min - gets the lowest value of the values in a given field per group.

// get the lowest number of stock for all items on sale
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"lowestStockOnSale", minStock:{$min: "$stocks"}}}

])

// lowest price for all item on sale:
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"lowestPriceOnSale", minPrice: {$min: "$price"}}}	

])

// Mini-Activity
// Get the lowest number stocks for all items
// which price is lower than 50

db.fruits.aggregate([

	{$match: {price:{$lt:50}}},
	{$group: {_id:"lowestStock",minStock:{$min:"$stocks"}}}

])

// Other Stages
// $count - is a stage added after $match stage to count all items that matches our criteria.

// Count all items on sale
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$count: "itemsOnSale"}

])

// Count all items whose price is less than 50
db.fruits.aggregate([

	{$match: {price:{$lt:50}}},
	{$count: "itemsPriceLessThan50"}

])

// Number of Items with stocks less than 20
db.fruits.aggregate([

	{$match: {stocks:{$lt:20}}},
	{$count: "forRestock"}

])

// $out - save/output the results in a new collection
// note: This will overwrite the collection if it already exists

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}},
	{$out: "stocksPerSupplier"}

])

/*
	Activity (aggregationDB)

	Aggregate to count the total number of items supplied by Yellow Farms and has a price lower than 50. ($count)

	Aggregate to count the total number of items with price lesser than 30. ($count)

	Aggregate to get the average price of fruits supplied by Yellow Farm. ($group)

	Aggregate to get the highest price of fruits supplied by Red Farm Inc. ($group)

	Aggregate to get the lowest price of fruits supplied by Red Farm Inc. ($group)

	save your query/commands in the activity.js

*/